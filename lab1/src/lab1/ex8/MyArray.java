package lab1.ex8;

import java.util.ArrayList;
import java.util.Random;

public class MyArray {
    public static void main(String[] args) {

        ArrayList<Integer> array = new ArrayList<Integer>(20);
        Random random = new Random();

        for (int i = 0; i<20; i++){
            array.add(random.nextInt(10));
            System.out.println(array.get(i).toString());
        }

        Integer sum = array.stream()
                .mapToInt(Integer::intValue)
                .sum();

        Integer avg = sum/array.size();
        System.out.println("average of array " + avg);




    }

}
