package lab1.ex10;

import java.util.Random;
import java.util.Vector;

public class MyMatrix {

    public static void main(String[] args) {
        Vector<Vector<Integer>>  matrix= new Vector<Vector<Integer>>();
        Random random = new Random();

        for(int i=0;i<3;i++){
            Vector<Integer> r=new Vector<>();
            for(int j=0;j<5;j++){
                r.add(random.nextInt(10));
            }
            matrix.add(r);
        }
        for(int i=0;i<3;i++){
            Vector<Integer> r=matrix.get(i);
            for(int j=0;j<5;j++){
                System.out.print(r.get(j) + " ");
            }
            System.out.println();
        }
    }
}
