package lab1.ex3;

public class Sequence {
    public static void main(String[] args) {

        float sum = 0;

        for (int i = 1; i < 7; i++) {
            sum += 1 / i;
        }

        System.out.println(sum);
    }
}
