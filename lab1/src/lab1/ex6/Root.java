package lab1.ex6;

import java.util.ArrayList;

public class Root {

    static double y1;
    static double y2;
    double a;
    double b;
    double c;

    static ArrayList<Double> roots = new ArrayList<Double>();

    public static ArrayList<Double> getRoots(double a, double b, double c) {

        double delta = (b * b)- (4*a*c);

        y1 = (-b + Math.sqrt(delta))/(2 * a);
        y2 = (-b - Math.sqrt(delta))/(2 * a);

        roots.add(y1);
        roots.add(y2);
        return roots;
    }

}
