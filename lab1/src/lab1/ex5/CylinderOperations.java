package lab1.ex5;

public class CylinderOperations {

    double pi = 22/7;

    public double getRadius(double d) {
        return d / 2;
    }

    public double getVolume(double d, double h){
        return pi * h * h * d;
    }

    public double getSurface(double d, double h){
        double r = getRadius(d);

        return 2*pi*r*r + 2*pi*r*h;
    }

    public double getPerimeter(double d, double h){
        return (2 * d) + (2 * h);
    }

    public double getLateralSurface(double d, double h){

        double r = getRadius(d);

        return 2*pi*r*h;
    }
}
